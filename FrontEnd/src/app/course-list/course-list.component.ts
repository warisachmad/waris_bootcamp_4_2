import { Component, OnInit } from '@angular/core';

import { CourseService } from '../service/course.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  constructor(private course:CourseService) { }

  ngOnInit() {
  }

  newNamaCourse : string = "";
  newSKS : string = "";
  newDosen : string = "";
  newTugas : string = "";

   addToDo(){

    this.course.courseList.push({ "nama_course":this.newNamaCourse, "sks":this.newSKS, "dosen_id":this.newDosen, "tugas_id":this.newTugas });

    this.newNamaCourse = "";
    this.newSKS = "";
    this.newDosen = "";
    this.newTugas = "";
  }

}
