import { Injectable } from '@angular/core';

@Injectable()
export class CourseService {

  constructor() { }

  courseList : Object[] = [
    { "id":1, "nama_course":"Fundamental", "sks":"3 sks", "dosen_id":"1", "tugas_id":"1" },
    { "id":2, "nama_course":"OOP", "sks":"2 sks", "dosen_id":"2", "tugas_id":"2" },
    { "id":3, "nama_course":"Database Design", "sks":"2 sks", "dosen_id":"3", "tugas_id":"3" },
    { "id":2, "nama_course":"Front End", "sks":"3 sks", "dosen_id":"4", "tugas_id":"4" },
    { "id":3, "nama_course":"Back End", "sks":"3 sks", "dosen_id":"5", "tugas_id":"5" }
  ];

}
