<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\mahasiswa;

class mahasiswaController extends Controller
{

    function getData(){

        $mahasiswa = mahasiswa::get();
        return response()->json($mahasiswa, 200);
    }

    function saveData(Request $request){
        DB::beginTransaction();

        try{
             $this->validate($request, [
                'nama' => 'required|max:255',
                'email' => 'required|email',
                'alamat' => 'required|max:255' 
              ]);

            $nama = $request->input('nama');
            $email = $request->input('email');
            $alamat = $request->input('alamat');

            // Save
            $mhs = new mahasiswa;
            $mhs->nama = $nama;
            $mhs->email = $email;
            $mhs->alamat = $alamat;
            $mhs->save();
            
            // Update
            $mhs = mahasiswa::where('nim', '=', 1)->first();
            $mhs->nama = $nama;
            $mhs->email = $email;
            $mhs->alamat = $alamat;
            $mhs->save();

            $mahasiswa = mahasiswa::get();

            DB::commit();

            return response()->json($mahasiswa, 201);
        }
        catch(\Exception $e){
            DB::rollback();

            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

}
